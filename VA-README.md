# Project Language and Setup

This project is written with Java 1.8 and packaged using Maven.

# Running the Project

The main class is MissionControl.  This can also be ran using maven via the command line and running 'mvn exec:java'.