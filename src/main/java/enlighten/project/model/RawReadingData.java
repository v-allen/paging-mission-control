package enlighten.project.model;

import java.time.LocalDateTime;

public class RawReadingData {
    private int id;
    private String component;
    private LocalDateTime timestamp;
    private float redHighLimit;
    private float redLowLimit;
    private float reading;

    public RawReadingData(int id, String component, LocalDateTime timestamp, float redHighLimit, float redLowLimit, float reading) {
        this.id = id;
        this.component = component;
        this.timestamp = timestamp;
        this.redHighLimit = redHighLimit;
        this.redLowLimit = redLowLimit;
        this.reading = reading;
    }

    public int getId() {
        return id;
    }

    public String getComponent() {
        return component;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public float getRedHighLimit() {
        return redHighLimit;
    }

    public float getRedLowLimit() {
        return redLowLimit;
    }

    public float getReading() {
        return reading;
    }
}
