package enlighten.project.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;
import java.util.Objects;

public class SatelliteAlert {
    private int satelliteId;
    private String component;
    private WarningType warningType;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
    @com.fasterxml.jackson.databind.annotation.JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime timestamp;

    public SatelliteAlert(int satelliteId, String component, WarningType warningType) {
        this.satelliteId = satelliteId;
        this.component = component;
        this.warningType = warningType;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public String getComponent() {
        return component;
    }

    public WarningType getWarningType() {
        return warningType;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SatelliteAlert that = (SatelliteAlert) o;
        return satelliteId == that.satelliteId &&
                Objects.equals(component, that.component) &&
                warningType == that.warningType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, component, warningType);
    }
}
