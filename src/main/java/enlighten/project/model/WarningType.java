package enlighten.project.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum WarningType {
    RED_HIGH("RED HIGH"),
    RED_LOW("RED LOW");

    private String prettyString;

    WarningType(String prettyString) {
        this.prettyString = prettyString;
    }

    @JsonValue
    public String getPrettyString() {
        return prettyString;
    }
}
