package enlighten.project.model;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TelemetryAlerts {
    Map<SatelliteAlert, List<LocalDateTime>> satelliteAlertHistory;

    public TelemetryAlerts() {
        satelliteAlertHistory = new HashMap<>();
    }

    public List<SatelliteAlert> getCriticalAlerts() {
        return satelliteAlertHistory.entrySet().stream()
                .filter(entry -> entry.getValue().size() >= 3)
                .map(entry -> {
                    entry.getKey().setTimestamp(entry.getValue().get(0));
                    return entry.getKey();
                }).collect(Collectors.toList());
    }

    public void determineAlertStatus(RawReadingData parsedData) {
        SatelliteAlert alert = null;
        if (parsedData.getReading() < parsedData.getRedLowLimit()) {
            alert = new SatelliteAlert(parsedData.getId(), parsedData.getComponent(), WarningType.RED_LOW);
        } else if (parsedData.getReading() > parsedData.getRedHighLimit()) {
            alert = new SatelliteAlert(parsedData.getId(), parsedData.getComponent(), WarningType.RED_HIGH);
        }

        if (alert != null) {
            List<LocalDateTime> previousTimes = satelliteAlertHistory.get(alert);
            previousTimes = comparePreviousReadings(previousTimes, parsedData.getTimestamp());
            satelliteAlertHistory.put(alert, previousTimes);
        }
    }

    private List<LocalDateTime> comparePreviousReadings(List<LocalDateTime> previousTimes, LocalDateTime dateTime) {
        if (previousTimes == null) {
            previousTimes = new LinkedList<>();
            previousTimes.add(dateTime);
            return previousTimes;
        }
        if (previousTimes.size() == 0) {
            previousTimes.add(dateTime);
            return previousTimes;
        }
        if (previousTimes.size() == 3) {
            return previousTimes;
        }

        long minutesDifference = ChronoUnit.MINUTES.between(previousTimes.get(0), dateTime);
        if (minutesDifference <= 5) {
            previousTimes.add(dateTime);
            return previousTimes;
        } else if (minutesDifference > 5) {
            previousTimes.remove(0);
            comparePreviousReadings(previousTimes, dateTime);
        }

        return previousTimes;
    }
}
