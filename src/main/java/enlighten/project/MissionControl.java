package enlighten.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import enlighten.project.model.RawReadingData;
import enlighten.project.model.TelemetryAlerts;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MissionControl {
    public static void main(String[] args) throws IOException {
        String fileLocation = "src/main/resources/inputData";

        TelemetryAlerts alerts = new TelemetryAlerts();

        try(FileReader file = new FileReader(fileLocation);
            BufferedReader reader = new BufferedReader(file)) {

            String line = reader.readLine();
            while (line != null) {
                String[] data = line.split("\\|");
                LocalDateTime dateTime = LocalDateTime.parse(data[0], DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS"));
                Integer id = Integer.parseInt(data[1]);
                Float redHighLimit = Float.parseFloat(data[2]);
                Float redLowLimit = Float.parseFloat(data[5]);
                Float reading = Float.parseFloat(data[6]);
                String component = data[7];

                RawReadingData parsedData = new RawReadingData(id, component, dateTime, redHighLimit, redLowLimit, reading);

                alerts.determineAlertStatus(parsedData);
                line = reader.readLine();
            }
        } catch (Exception error) {
            System.err.println(error.getMessage());
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(alerts.getCriticalAlerts()));
    }
}
