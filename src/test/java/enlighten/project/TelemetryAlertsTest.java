package enlighten.project;

import enlighten.project.model.RawReadingData;
import enlighten.project.model.SatelliteAlert;
import enlighten.project.model.TelemetryAlerts;
import enlighten.project.model.WarningType;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class TelemetryAlertsTest {
    private SatelliteAlert alert = new SatelliteAlert(1, "test", WarningType.RED_HIGH);

    private RawReadingData reading1 = new RawReadingData(1, "test", LocalDateTime.of(2021, 1, 1, 8, 30, 0), 10, 5, 11);
    private RawReadingData reading2 = new RawReadingData(1, "test", LocalDateTime.of(2021, 1, 1, 8, 32, 0), 10, 5, 15);
    private RawReadingData reading3 = new RawReadingData(1, "test", LocalDateTime.of(2021, 1, 1, 8, 34, 0), 10, 5, 12);

    private RawReadingData readingLater1 = new RawReadingData(1, "test", LocalDateTime.of(2021, 1, 1, 8, 40, 0), 10, 5, 13);
    private RawReadingData readingLater2 = new RawReadingData(1, "test", LocalDateTime.of(2021, 1, 1, 8, 42, 0), 10, 5, 11);
    private RawReadingData readingLater3 = new RawReadingData(1, "test", LocalDateTime.of(2021, 1, 1, 8, 44, 0), 10, 5, 18);

    private RawReadingData readingDiffDay = new RawReadingData(1, "test", LocalDateTime.of(2021, 1, 2, 8, 44, 0), 10, 5, 18);

    @Test
    void zeroAlerts() {
        TelemetryAlerts alerts = new TelemetryAlerts();
        alerts.determineAlertStatus(reading1);
        assert(alerts.getCriticalAlerts().size() == 0);
    }

    @Test
    void oneAlert() {
        TelemetryAlerts alerts = new TelemetryAlerts();
        alerts.determineAlertStatus(reading1);
        alerts.determineAlertStatus(reading2);
        alerts.determineAlertStatus(reading3);
        assert(alerts.getCriticalAlerts().size() == 1);

        SatelliteAlert alert = alerts.getCriticalAlerts().get(0);
        assert(this.alert.equals(alert));
        assert(alert.getTimestamp().equals(reading1.getTimestamp()));
    }

    @Test
    void zeroAlertDueToLaterReading() {
        TelemetryAlerts alerts = new TelemetryAlerts();
        alerts.determineAlertStatus(reading1);
        alerts.determineAlertStatus(reading2);
        alerts.determineAlertStatus(readingLater1);
        assert(alerts.getCriticalAlerts().size() == 0);
    }

    @Test
    void oneAlertFromLaterReadings() {
        TelemetryAlerts alerts = new TelemetryAlerts();
        alerts.determineAlertStatus(reading1);
        alerts.determineAlertStatus(reading2);
        alerts.determineAlertStatus(readingLater1);
        alerts.determineAlertStatus(readingLater2);
        alerts.determineAlertStatus(readingLater3);
        assert(alerts.getCriticalAlerts().size() == 1);

        SatelliteAlert alert = alerts.getCriticalAlerts().get(0);
        assert(this.alert.equals(alert));
        assert(alert.getTimestamp().equals(readingLater1.getTimestamp()));
    }

    @Test
    void zeroAlertsDueToDifferentDays() {
        TelemetryAlerts alerts = new TelemetryAlerts();
        alerts.determineAlertStatus(reading1);
        alerts.determineAlertStatus(readingDiffDay);

        assert(alerts.getCriticalAlerts().size() == 0);
    }
}
